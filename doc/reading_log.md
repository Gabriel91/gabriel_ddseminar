2016-01-29 Bengio et al., 2003

The main goal of the paper is learning the conditional probability of the next word in a text given the previous words. To do so, the authors associate each word in the vocabulary with a vector in R^m called the word feature vector. Then, the joint probability of a sequence of words is expressed only in terms of the corresponding word feature vectors. The idea behind doing this is reducing the dimension of the word space: similar words should have feature vectors that are close to each other. The feature map and the joint probability are learned together by using a neural network. The authors also describe a way to parallelise the training to make it scalable. The proposed model is then successfully compared to an n-gram model, yielding better results. Some discussion points I thought about:

-m is always treated as a fixed parameter, it could be interesting to think of ways to make the data tell us what m should be.

-I am not aware of a bayesian procedure that attempts to do the same thing, that is, find the word feature map and then express the joint probability in terms of the feature map. It would be interesting to think of such a model, since it would share the strength of estimating the feature map and the joint probability simultaneously and would likely have scalable variational inference algorithms. Also, in the future work section, the authors suggest that incorporating a-priori knowledge could make the model better, and the bayesian approach would seem like a very natural way to do so.

2016-02-05 Mikolov et al., 2014

This paper extends the skip-gram model to handle embeddings for phrases instead of just words. The idea behind doing so is that the meaning of some phrases does not correspond to the meanings of the words in the phrase, for example, "New York Times" shouldn't be decomposed in three words "New", "York" and "Times", but should instead have its own embedding. To do so, the authors define a score to automatically detect whether words should be put together in a phrase or not.

2016-02-05 Pennington et al., 2014

The authors make the argument that instead of looking at Pij, the probability of word i appearing in the context of word j, we should look at ratios of the form Pik/Pjk. They propose some properties that should hold for the word embeddings wi, namely wi^Twk+bi+bk=log(1+Xik) where Xik is the number of times word j appears in the context of word i. To adjust the model, they optimize a weighted sum of [wi^Twj+bi+bj-log(1+Xij)]^2 where the weights f(Xij) have some properties, most notably that f(0)=0. This property allows to take advantage of sparcity in the calculations since most Xij are 0.

2016-02-05 Arora et al., 2015

This paper proposes a generative model for text data by building on word embeddings. The aim is to recover words embeddings in such a way that the mutual information between two words is roughly equal to the inner product of their corresponding embeddings. Thus, big inner products reflect that words are similar, inner products near 0 reflect that words are independent and negative inner products reflect that words are very different. The model assumes an underlying Markov Chain whose space state is R^d where d is the dimension of the word embeddings. The state of the Markov Chain represents what is being talked about, and the probability of the t-th word given the state of the chain is proportional to the exponential of the inner product between the corresponding word embedding and the current state of the chain. This way, words that are similar to what is being talked are the words with high probability. The authors also prove that if d is low dimensional and under some assumptions on the Markov Chain and the prior for the word embeddings, their procedure is such that the inner product of word embeddings is actually close to the mutual information. Some thoughts I had about the paper:

-I think that having the inner product being similar to the mutual information is a great idea, even though the idea was not introduced in this paper I was not aware of it and really liked it.

-I also really liked the generative model, it seems very reasonable to have the vector of what is being talked about. The authors don't go into much detail on how to chose the Markov Chain, they just say it should mix slowly and have uniform stationary distribution. However, a scenario could happen where there are a couple of sudden changes of topic in a document, which could not be captured by this model since the chain is slowly mixing. Thinking about ways to handle this could be interesting.

2016-02-19 Arora et al., 2016

The main purpose of this paper is to address the issue of polysemus words (i.e. words with multiple meanings) when doing word embeddings. The authors explain why if a word with several meanings was replaced by separate words for each of its meanings and a word embedding was obtained, the embedding for the original word should approximately be a linear combination of the embeddings of the new artificial words. What the authors propose is doing sparse coding to the already learned word embeddings. Given a set of word embeddings, sparse coding returns a set of vectors such that each word can be approximately written as a linear combination of sparsely many vectors in the sparse coding. Then, the vectors in the sparse coding involved in the representation of the embedding of a word can be thought of as the different meanings that word has. Some thoughts I had when reading the paper:

-The first thought that crossed my mind after reading the abstract and seeing that they wanted to deal with polysemous words was that the authors were going to propose a model in which each word has several embeddings, each corresponding to one of its meanings, and then each time the word appeared a specific embedding was chosen. What I really like about how they deal with this is that they don't make a more complicated model like the one I just mentioned, they just learn the different meanings of a word by analysing its word embedding and that of other words. I found it very intereseting that one can recover the different meanings of a word only by observing the embedding corresponding to the "combined meaning" of the word.

-Even though the paper was nice, I would like to know how much is gained by doing this. What percentage of words are polysemous?

2016-02-19 Levy and Goldberg, 2014

In this paper the authors show that SGNS (Skip-Gram with Negative Sampling), a popular word embedding method proposed by Mikolov et al. is equivalent to a matrix factorization problem, where the matrix we are trying to factorize is the PMI matrix shifted by a constant. They also propose other matrices that could be factorized and compare results.

2016-02-19 Le and Mikolov, 2014

In this paper the authors assign an embedding not only to each word but also to each paragraph (words have the same embedding throughout paragraphs). They do this in a way that the documents are not treated as a bag of words. They use their method to predict next words in documents and show good results. I thought it could be intersting to link the paragraph embedding with the vector representing what is being talked about from Arora's paper.

2016-02-26 Bhattacharya and Dunson, 2012

The authors propose a model which can be though of as an extension of LDA and can be interpreted a tensor factorisation. Their main result is that when the sample size goes to infinity, the posterior concentrates around the "true" parameter, that is, the estimation is consistent. They derive an inference algorithm which involves sampling and runs in linear time (I did not completely follow why this is true, looking forward to understand it during class). I thought the paper was very interesting, and I liked the statistical approach which is not often found in machine learning papers. I think proving consistency of their method is an important theoretical guarantee that should be done, or at least attempted, more often.

2016-02-26 Zhou et al., 2014

They propose another tensor factorization model which is similar to the one used in the previous paper. Once again, they prove that the posterior concentrates where it should and give algorithms for posterior inference which run in linear time.

2016-03-04 Hoffman et al., 2013

I was already familiar with SVI. The goal is making VI faster, and to do this the main idea is noticing that at each iteration of coordinate ascent for VI, a sum over the complete dataset is involved. Stochastic optimization can then be used by realizing that a noisy estimate of the gradient can be obtained by summing over a small random subset of the data (and scaling accordingly). This makes the optimization algorithm much faster. The other important point of SVI is the use of the natural gradient instead of the gradient, to make. I really like this paper, I would consider very interseting extendingthis to when:
-We don't have conjugacy (which I know is done in the next paper)
-We don't use the mean field family, allowing us to better capture correlation structure. I'm aware of some proposals for this, but as far as I know it's something people are still thinking about.
-We don't have conditional independence. I'm not sure if work on performing SVI and not just VI on this setting has been done.

2016-03-04 Ranganath et al., 2014

This paper deals with the problem of doing SVI when we don't have conjugacy. The idea is using a Monte Carlo estimate of the gradient instead of calculating it exactly. I thought this is a very elegant solution, so simple it can be described in a sentence, yet very useful and practical.

2016-03-04 Blei et al., 2016

This is just an introduction to VI and SVI with some illustrative examples. I liked that in the open problems part it's mentioned that the mathematical properties of variational methods are not well understood. I think this is something well known but often overlooked, and as a statistician not having theoretical guarantees bothers me.

2016-03-04 Kingma and Welling, 2013

In this paper the authors do something very similar to black box VI, except the approximating family's parameters don't necessarily have closed form updates since they are given by the output of a neural network. They use a change of variable to be able to compute the Monte Carlo estimates of the gradient for the updates.

2016-03-04 Mohamed, 2015

In this post the author explains the reparametrization trick, which is just a change of variable and it's use for simulating random variables and on the previous paper. I thought the water flowing explanation was not good, and he does not do a better job at explaining the trick than Kingma and Welling's paper.

2016-03-11 Taddy, 2015

This paper proposes a method for parallelized in multinomial regression models where the probabilities of categories are given by normalized exp(linear function). They do this by noticing that they can fit a poisson regression for each category and then put all the results together. To do this they use a plu-in estimator for a parameter in the poisson regressions. The authors then test their method on a yelp review dataset. I liked the paper, I was not familiar with multinomial regression, and I'm not used of thinking about regression as a tool for big datasets. I thought this was a cool way of dealing with that. I would have liked to see comparisons against other text analysis methods though.

2016-03-11 Taddy, 2013

In this paper the author proposes a model for text data. He proposes a multinomial regression, but instead of using all the data the regression is over a lower dimensional space. He uses independent Laplace priors to have L1 regularization. However, each component has its own parameter, instead of all sharing the same one. I was not familiar with this, and the athor claims it helps avoid overpenalizing some coefficients. He also provides an algorithm for performing MAP inference. He then applies this to a couple examples and compares results with other text analysis methods. It's interesting that the model allows for including other data via the rehression other than just the text. I liked the paper, however I don't understand why it's not common practice in statistics to write the generative model but instead specify the likelihood and the prior in different parts of the paper (and this is comming from a statistician), it would make things clearer.

2016-03-11 Rabinovich and Blei, 2014

This paper combines LDA and the model used in the previous paper to have the advanteges of both: exploiting structure in the text and including covariates, respectively. The authors derive variational algorithms to perform inference. I thought this was an interesting paper, and felt it was clearer that Taddy's paper.

2016-03-25 Blei and Gershman, 2013

I was already familiar with the material covered here, so I thought it was a bit simple. However I thought the explanation about why the CRP is exchangeable was very good and easy to follow.

2016-03-25 Neal, 2000

I thought this was a very well written paper. I was not familiar with how to sample from DPs and Neal makes a great job at explaining how to do so. I am also not familiar with how VI works for DPs and would like to learn about it.

2016-03-25 Jordan

I was also familiar with the material presented here thanks to the reading group on Poisson Processes from last semester. There was a quote that I thought was very interesting: "only an optimist could hope to fit a model involving an infinite number of degrees of freedom based on finite data. But it also expresses the pessimism that simplified paraetric models may be inadequate to capture many real-world phenomena".

2016-03-25 Teh and Jordan, 2010

Although it took me a while to read, I enjoyed this very much. I was more or less familiar with the theory behind some Bayesian non-parametric models, but had only seen few applications (not saying there are few applications, I know there are a lot, I just don't know most of them). I like seeing how knowing the theory behind these stochastic processes and their properties helps building models that are adecuate for the task at hand. For example, knowing that the number of clusters in a DP grows logarithmically allows us to have a good idea of when it can be convenient to use a DP. I also liked reading about HDP-LDA and HDP-HMM.

2016-03-25 Lecture notes from FOGM

This felt too simple after the previous two papers, but I think it highlights all the important points.

2016-04-01 Zhou and Carin, 2015

I got confused when reading this paper. I'm not familiar with all of the stochastic processes they introduce at the beginning and I felt that if one doesn't now the processes the introduction is not enough. For example, the Gamma process has Levy measure r^-1exp(-cr)drdG0(dw), which doesn't integrate to something finite. I don't understand why that is not an issue when the "expected value" of r is still finite. I'm really looking forward to class though, as I would really like to understand everything that's going on.

2016-04-15 Braun and McAuliffe, 2010

In this paper the authors derive variational inference for a logistic choice model. I thought it was well written, but it almost felt like just applyng variational inference to a dataset. I liked that they talk about the convexity properties of their problem (although not jointly convex, the update for each coordinate is a smooth unconstrained convex optimization problem) in the appendix; I think there is a habit optimizing functions without stopping to consider if they are convex or not. I understand that if they are not convex we may not be able to do much and doing the optimization is better than nothing, but we should at least be aware that we might not be getting the real optimum.

2016-04-22 Hoff et al., 2002

The authors propose two simple logistic models for graph data: one including distance between two latent vectors and one including angle (not exactly angle, but that's the spirit). They then apply their models to some fun small datasets. A direction I think would be intersting is consider not only binary data but also count data, for example, instead of measuring if person i is close to person j, measuring the number of interactions between persons i and j. Also, their method seems to only be applicable to small datasets. As far as I understand making graph models that work for large datasets is an open problem though.