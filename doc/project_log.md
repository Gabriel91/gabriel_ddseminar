2016-02-05

I still have two options for my project: analysing the yelp dataset or doing something involving neuroscience. I set up a meeting with John Cunningham to talk about the neuroscience project and I also started to process the yelp dataset. It was in a format called json that I was not familiar with so I had to learn how to read json files. I am also not familiar with the fastest way to build the vocabulary and the word counts, so I made to program to do so which is currently still running on my computer. I need to learn how to do so in a server to make it more efficient. Depending on what happens at the meeting I will chose which project to continue.

2016-02-19

After meeting with John, I decided to not continue the Yelp project and focus instead in a project involving neuroscience. There are still two options and I've been doing some reading on both of them. The first one is using deterimantal point processes to model neural data. I read some backround about these point proceses, which are an extension of Poisson processes that allow to model negative correlations. This can be useful for modelling neural data because sometimes neurons compete with each other, so knowing that a certain neuron went off means it's less likely that competing neurons also went off. For the second one, we have a dataset consisting of observations of N neurons in C conditions at T times. The goal is to determine if the structure we observe in the data would be observed in any "neural looking dataset" or if there is something else going on.

2016-02-26

I am working on the second neuroscience project I mentioned last time. Remember our data consists of observations of N neurons in C conditions at T times. Using this data, a very common model that is fitted is a low dimensional dynamical system. These models usually fit the data quite well, however there is a debate about whether the fit is good because something meaningful is going on in the brain or simply because the data is high dimensional and complex and we should always expect good fits from these low dimensional models when using "neural looking data".

What has been done to answer this question is the following: Suppose that we have K trials of our NxCxT data. Smooth over trials, calculate the "marginal covariances" of the smoothed data with respect to T, N and C, respectively Sigma_T, Sigma_N and Sigma_C (assume the data is centered). It is assumed that "neural looking data" shares this covariance structure with our data. Let p be the maximum entropy distribution subject to the covariance constraints. p turns out to be normal and its parameters can be exactly calculated. The goal is now to do a hipothesis test: does our data come from p or not? To do the hipothesis test, we compare the R^2 of the fitted model of our data against that of simulated data from p. If the R^2s are similar, this means that we would expect the observed behavior from "neural looking data", otherwise, something cool is actually going on in our brain. The results indicate that there is actually something more going on than just good fits because of high dimensionality.

The goal of my project is performing the same analysis without smoothing over trials. There are a couple of reasons why smoothing is not the most desirable thing to do, so it would be very exciting to obtain the same results without smoothing as it would be stronger evidence supporting this observed brain behaviors. I am currently working on deriving the equivalent maximum entropy distribution, which is of course not normal anymore.

2016-02-27

I have been working on deriving the maximum entropy distribution. I think I have it, but it is not clear at all how to sample from it. In the continuous case the distribution is normal and although it's very high dimensional, can be sampled efficiently using tensor algebra tricks. I've been thinking about how to use the samples from the normal to obtain the discrete samples while preserving the correlation structure, which would allow to sample efficiently from it.

2016-03-01

I realized a mistake in what I did last time, even though the form of the maximum entropy distribution is easily derived, its parameters are not. I've spent several hours trying to get them without success. I had discussed the possibility of using a similarity metric other than the covariance to represent the structure of "neural looking data" with John as a next step once this is done in the project. I am trying to find a metric that would be both meaningful and make the optimization feasible and use it instead of the covariance.

2016-03-02

I have been reading on Kronecker matrix algebra, which is used in the continuous case to derive both the maximum entropy distribution and the efficient sampling from it, to see if I can do something similar for the discrete case. I realized that given a covariance structure, I would not know how to simulate from a multivariate bernoulli distribution with that covariance, so I've been reading on methods involving copulas that can do that.

2016-03-10

I kept thinking about the same issues and have not been able to fix them. I met with John again and he suggested that I learn about spin glasses. Spin glasses are models used in physics for atoms' spin. The spin value of each atom is binary and spins are correlated between each other, which is similar to this multivariate bernoulli distribution that I have to find by maximizing the entropy. I have been reading about spin glasses trying to find something usefull for my project. As a side note, I was just feeling somewhat amazed by all the relations between physics and statistics: Monte Carlo methods were first developped by physicist and Hamiltonian Monte Carlo is based in physical principles; and now I found out there is also a conection with spin glasses.

2016-03-25

I found a method that allows to simulate from multivariate bernoulli distributions with specified marginals and covariance. I am still not sure if it actually corresponds to the maximum entropy distribution, since the maximum entropy distribution subject to the covariance constraints should not exhibit any higher order interactions and I'm not sure if the variables generated from this method have or not higher order interactions. Note that this is not an issue for the continous case as the normal distribution is completely caracterized by its mean and variance and can't have higher order interactions. I have spent a good amount of time trying to prove or disprove if the method induces higher order interactions, but have not been successful. To get an empirical answer, I chose arbitrary covariance structure and used the method to simulate bernoulli data with the specified covariance and checked for higher order interactions. The results seem to indicate that there are no higher order interactions, but I still need to keep testing. The paper describing the method I keep talking about is called "Simulating dependent discrete data" (call it[1]) by L. Madsen and D. Birkes in the Journal of Statistical Computation and Simulation. I have also spend some time reading about spin glasses.

2016-04-01

I kept reading about spin glasses and don't think they can be helpfully applied to my problem. I discovered a mistake in my previous code and in reality things were not working as I thought they were. I have kept running simulations and can't get the method described in [1] to actually work. However they prove it works and the proof convinces me, so I'm quite frustrated, I hope to figure out what I'm doing wrong soon.

2016-04-07

The simulations are working, I figured out how to sample from the maximum entropy distribution for the discrete case with marginal and covariance constraints. The reason why it wasn't working before is because not all pairs of marginals and correlations are feasible, that is, if we fix the marginal distributions, there might exist correlations that no probability distribution with the desired marginals can have. I will describe how to simulate from the maximum entropy multivariate Bernoulli with mean vector mu and correlation matrix Sigma:
1) Generate Z from N(0,Delta).
2) Let Ui=Phi(Zi), where Phi is the standard normal cdf; so that Ui is uniform in (0,1).
3) Let Yi=Fi^(-1)(Ui) where Fi^(-1) is the "inverse" cdf of a Bernoulli with mean mui; so that Yi is Bernoulli mui.
Where Delta is chosen so that the correlation of Y is Sigma. Delta can easily be recovered when the dimension of Y is not too big, but for my neural data the dimension is around 10^6. I have started to think how to come up with a fast way to approximate Delta such that the approximation is structured in such a way that sampling from N(0,Delta) will be easy.

2016-04-15

Even if the simulations are working, I am trying to prove that they should work, that is, that the method I previously described actually simulates from the maximum entropy distribution that I want to sample from. I have been working on the proof and hope to have it soon.

2016-04-29

I have been running more simulations and things appear to work, I have not yet figured out how to scale this for th 10^6 dimesional case though. I also began typing the final report.